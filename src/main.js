var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var steps = 50
var font

function preload() {
  font = loadFont('ttf/RobotoMono-Medium.ttf')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  textFont(font)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < steps; i++) {
    push()
    translate(windowWidth * 0.5 + sin(frameCount * 0.025 + i * 0.075) * boardSize * 0.175, windowHeight * 0.5 + (1 / steps) * boardSize * (i - Math.floor(steps * 0.5)) * 0.75)
    textSize(boardSize * 0.1 * abs(sin(frameCount * 0.01 + i * 0.05)))
    textAlign(CENTER, CENTER)
    strokeWeight(boardSize * abs(sin(frameCount * 0.01 + i * 0.05)) * 0.01)
    stroke(((255 / (steps + 1)) * (Math.floor(frameCount + i) % steps)) * (i / steps))
    fill(0)
    text('([{._.}])', 0, 0)
    pop()
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
